//
//  ProgressSubmitButton.swift
//  Pods
//
//  Created by Hiroshi Kimura on 2/5/16.
//
//

import Foundation
import UIKit
import Animo

public class ProgressSubmitButton: UIButton {
    
    public enum State {
        case Normal
        case Submitting(NSProgress?)
        case Completed
    }
    
    public var submitState: State = .Normal {
        didSet {
        
            switch (oldValue, self.submitState) {
            case (.Normal, .Normal), (.Submitting, .Submitting), (.Completed, .Completed):
                break
            default:
                self.updateLayer(self.submitState)
            }
        }
    }
    
    public convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    public override func intrinsicContentSize() -> CGSize {
        let originalSize = super.intrinsicContentSize()
        var size = originalSize
        size.height += 16
        size.width += 80
        return size
    }
    
//    public override func setTitle(title: String?, forState state: UIControlState) {
//        super.setTitle(title, forState: state)
//    }
//    
//    public override func setBackgroundImage(image: UIImage?, forState state: UIControlState) {
//        //
//    }
    
    public override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        if let object = object as? NSProgress where keyPath == "fractionCompleted" {
            self.progressLayer.runAnimation(
                Animo.keyPath("strokeEnd", from: nil, by: nil, to: object.fractionCompleted, duration: 0.3, timingMode: TimingMode.EaseInOut, options: Options())
            )
        }
        
    }
    
    public override var tintColor: UIColor! {
        didSet {
        // TODO
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    public override func layoutSublayersOfLayer(layer: CALayer) {
        super.layoutSublayersOfLayer(layer)
        if self.borderLayer.bounds != layer.bounds {
            self.updateLayer(self.submitState)
        }
    }
    
    // MARK: - Private
    
    private let borderLayer = CAShapeLayer()
    private let progressLayer = CAShapeLayer()
    private lazy var checkImageView: UIImageView = { [unowned self] in
        
        let image = UIImage(
            named: "progress_submit_button_checkmark",
            inBundle: NSBundle(forClass: self.dynamicType),
            compatibleWithTraitCollection: nil)?.imageWithRenderingMode(.AlwaysTemplate)
        
        return UIImageView(
            image: image, highlightedImage: image
        )
    }()
    
    private func setup() {
        
        assert(self.buttonType == .Custom, "ProgressSubmitButton: Please use buttonType 'Custom'")
        
        do { // Appearance
        
            self.tintColor = UIColor(red:0.1, green:0.8, blue:0.59, alpha:1)
            
            self.borderLayer.fillColor = UIColor.clearColor().CGColor
            self.borderLayer.lineWidth = 2
            self.borderLayer.strokeColor = self.tintColor.CGColor
            self.borderLayer.path = self.createNormalBorderPath().CGPath
            self.layer.addSublayer(self.borderLayer)
            
            self.progressLayer.fillColor = UIColor.clearColor().CGColor
            self.progressLayer.lineWidth = 2
            self.progressLayer.strokeColor = self.tintColor.CGColor
            self.progressLayer.opacity = 0
            self.progressLayer.path = UIBezierPath().CGPath
            self.layer.addSublayer(self.progressLayer)
            
            self.checkImageView.center = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
            self.checkImageView.autoresizingMask = [.FlexibleTopMargin, .FlexibleRightMargin, .FlexibleLeftMargin, .FlexibleBottomMargin]
            self.checkImageView.tintColor = UIColor.whiteColor()
            self.addSubview(self.checkImageView)
        }
        
        do { // Handling
        
            self.addTarget(self, action: "touchDown:", forControlEvents: .TouchDown)
            self.addTarget(self, action: "touchUpInside:", forControlEvents: .TouchUpInside)
            self.addTarget(self, action: "touchCancel:", forControlEvents: .TouchCancel)
        }
        
 
    }
    
    private func createNormalBorderPath() -> UIBezierPath {
        let path = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.bounds.height / 2)
        return path
    }
    
    private func createSubmittingBorderPath() -> UIBezierPath {
        var rect = self.bounds
        rect.origin.x = (rect.size.width / 2) - (rect.size.height / 2)
        rect.size.width = rect.height

        let path = UIBezierPath(roundedRect: rect, cornerRadius: self.bounds.height / 2)
        return path
    }
    
    private func createCircleProgressPath() -> UIBezierPath {
        let path = UIBezierPath(
            arcCenter: CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2),
            radius: self.bounds.height / 2,
            startAngle: CGFloat(0 - M_PI_2),
            endAngle: CGFloat((M_PI * 2) - M_PI_2),
            clockwise: true
        )
        return path
    }
    
    // MARK: Animation
    
    private func updateLayer(state: State) {
        
        switch state {
        case .Normal:
            
            self.borderLayer.runAnimation(
                Animo.group(
                    Animo.keyPath(
                        "path",
                        from: nil,
                        by: nil,
                        to: self.createNormalBorderPath(),
                        duration: 0.8,
                        timingMode: TimingMode.EaseOutCirc,
                        options: Options(fillMode: .Forwards)
                    ),
                    Animo.keyPath(
                        "fillColor",
                        from: nil,
                        by: nil,
                        to: UIColor.clearColor(),
                        duration: 0.8,
                        timingMode: TimingMode.EaseIn,
                        options: Options(fillMode: .Forwards)
                    )
                )
            )
            
            self.titleLabel?.layer.runAnimation(
                Animo.fadeIn(duration: 0.2)
            )
            
            self.checkImageView.layer.runAnimation(
                Animo.fadeOut(duration: 0.1)
            )
            
            self.progressLayer.path = self.createCircleProgressPath().CGPath
            self.progressLayer.strokeEnd = 0
            
        case .Submitting(let progress):
            
            if let progress = progress {
                progress.addObserver(self, forKeyPath: "fractionCompleted", options: NSKeyValueObservingOptions.New, context: nil)
            }
            
            self.borderLayer.runAnimation(
                Animo.group(
                    Animo.sequence(
                        Animo.wait(0.7),
                        Animo.keyPath(
                            "path",
                            to: self.createSubmittingBorderPath(),
                            duration: 0.8,
                            timingMode: TimingMode.EaseInOutCirc,
                            options: Options(fillMode: .Forwards)
                        )
                    ),
                    Animo.sequence(
                        Animo.keyPath(
                            "fillColor",
                            to: self.tintColor,
                            duration: 0.5,
                            timingMode: TimingMode.EaseIn,
                            options: Options(fillMode: .Forwards)
                        ),
                        Animo.group(
                            Animo.keyPath(
                                "strokeColor",
                                to: UIColor.lightGrayColor(),
                                duration: 0.3,
                                timingMode: TimingMode.EaseIn,
                                options: Options(fillMode: .Forwards)
                            ),
                            Animo.keyPath(
                                "fillColor",
                                to: UIColor.clearColor(),
                                duration: 0.3,
                                timingMode: TimingMode.EaseIn,
                                options: Options(fillMode: .Forwards)
                            )
                        )
                    )
                )
            )
            
            self.progressLayer.runAnimation(
                Animo.sequence(
                    Animo.wait(1.5),
                    Animo.fadeIn(duration: 0.2)
                    )
            )
            
            UIView.animateWithDuration(0.2, animations: { 
                
                self.highlighted = true
            })
            
            self.titleLabel?.layer.runAnimation(
                Animo.group(
                    Animo.sequence(
                        Animo.wait(0.23),
                        Animo.autoreverse(Animo.scale(to: 0.9, duration: 0.13, timingMode: TimingMode.EaseInOutExpo, options: Options())),
                        Animo.fadeOut(duration: 0.2)
                    )
                )
            )
            
        case .Completed:
            
            self.progressLayer.runAnimation(
                Animo.fadeOut(duration: 0)
            )
            
            self.borderLayer.runAnimation(
                Animo.group(
                    Animo.keyPath(
                        "path",
                        from: nil,
                        by: nil,
                        to: self.createNormalBorderPath(),
                        duration: 0.5,
                        timingMode: TimingMode.EaseInOutCubic,
                        options: Options(fillMode: .Forwards)
                    ),
                    Animo.keyPath(
                        "strokeColor",
                        to: self.tintColor,
                        duration: 0.3,
                        timingMode: TimingMode.EaseIn,
                        options: Options(fillMode: .Forwards)
                    ),
                    Animo.keyPath(
                        "fillColor",
                        from: nil,
                        by: nil,
                        to: self.tintColor,
                        duration: 0.5,
                        timingMode: TimingMode.EaseIn,
                        options: Options(fillMode: .Forwards)
                    )
                )
            )
            
            UIView.animateWithDuration(0.2, animations: {
                
                self.highlighted = false
            })
            
            self.checkImageView.layer.runAnimation(
                Animo.group(
                    Animo.fadeIn(duration: 0.1),
                    Animo.scale(
                        from: 0.6,
                        to: 1,
                        duration: 0.4,
                        timingMode: TimingMode.EaseInOutCubic,
                        options: Options()
                    )
                )
            )
        }
    }
    
    // MARK: - Private : Tap handling
    
    private dynamic func touchDown(sender: ProgressSubmitButton) {
        
        self.borderLayer.runAnimation(
            Animo.keyPath(
                "fillColor",
                from: nil,
                by: nil,
                to: self.tintColor,
                duration: 0.3,
                timingMode: TimingMode.EaseInOutCirc,
                options: Options(fillMode: .Forwards)
            )
        )

    }
    
    private dynamic func touchUpInside(sender: ProgressSubmitButton) {
        
        self.borderLayer.runAnimation(
            Animo.keyPath(
                "fillColor",
                from: nil,
                by: nil,
                to:  UIColor.clearColor(),
                duration: 0.3,
                timingMode: TimingMode.EaseInOutCirc,
                options: Options(fillMode: .Forwards)
            )
        )
    }
    
    private dynamic func touchCancel(sender: ProgressSubmitButton) {
        
        self.borderLayer.runAnimation(
            Animo.keyPath(
                "fillColor",
                from: nil,
                by: nil,
                to:  UIColor.clearColor(),
                duration: 0.3,
                timingMode: TimingMode.EaseInOutCirc,
                options: Options(fillMode: .Forwards)
            )
        )
    }
    
}