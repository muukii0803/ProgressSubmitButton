//
//  ViewController.swift
//  ProgressSubmitButton
//
//  Created by muukii on 02/05/2016.
//  Copyright (c) 2016 muukii. All rights reserved.
//

import UIKit
import ProgressSubmitButton

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = ProgressSubmitButton()
        self.view.addSubview(button)
        // Do any additional setup after loading the view, typically from a nib.
        
        do {
            let title = NSAttributedString(
                string: "Submit",
                attributes: [
                    NSFontAttributeName : UIFont(name: "AvenirNext-DemiBold", size: 15)!,
                    NSForegroundColorAttributeName : self.button.tintColor,
                ]
            )
            
            self.button.setAttributedTitle(title, forState: .Normal)
        }
        
        do {
            
            let title = NSAttributedString(
                string: "Submit",
                attributes: [
                    NSFontAttributeName : UIFont(name: "AvenirNext-DemiBold", size: 15)!,
                    NSForegroundColorAttributeName : UIColor.whiteColor(),
                ]
            )
            self.button.setAttributedTitle(title, forState: .Highlighted)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var button: ProgressSubmitButton!
    @IBAction func handleButton(sender: ProgressSubmitButton) {
        
        let progress = NSProgress(totalUnitCount: 10)
        
        sender.submitState = .Submitting(progress)
        
        self.after(0.8) {
            progress.completedUnitCount = 3
            self.after(0.8) {
                progress.completedUnitCount = 6
                self.after(0.8) {
                    progress.completedUnitCount = 10
                    sender.submitState = .Completed
                    
                    self.after(1) {
                        sender.submitState = .Normal
                    }
                }
            }
        }
    }
    
    func after(after: Double, block: () -> Void) {
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(after * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            
            block()
        }
    }
}

