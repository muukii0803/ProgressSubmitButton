# ProgressSubmitButton

[![CI Status](http://img.shields.io/travis/muukii/ProgressSubmitButton.svg?style=flat)](https://travis-ci.org/muukii/ProgressSubmitButton)
[![Version](https://img.shields.io/cocoapods/v/ProgressSubmitButton.svg?style=flat)](http://cocoapods.org/pods/ProgressSubmitButton)
[![License](https://img.shields.io/cocoapods/l/ProgressSubmitButton.svg?style=flat)](http://cocoapods.org/pods/ProgressSubmitButton)
[![Platform](https://img.shields.io/cocoapods/p/ProgressSubmitButton.svg?style=flat)](http://cocoapods.org/pods/ProgressSubmitButton)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ProgressSubmitButton is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ProgressSubmitButton"
```

## Author

muukii, m@muukii.me

## License

ProgressSubmitButton is available under the MIT license. See the LICENSE file for more info.
